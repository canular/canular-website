---
title: L'équipe CANULAR
subtitle: 
comments: false
Params.bigimg: [{src: "./equipeCanular.png", desc: "Path"}]
---



### L'équipe CANULAR

- Christelle Aluome **CATI PROSODIe**
- Christophe Chipeaux **CATI DIISCICO**
- Nicolas Devert **Testeur**
- Catherine Lambrot **Testeur**
- Tovo Rabemanantsoa **CATI PROSODIe**
- **Stéphane Thunot** **CATI GEDEOP**
****
![Equipe](./equipeCanular.png "Structure de l'équipe")
