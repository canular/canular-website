---
title: CANULAR
subtitle: Présentation du projet 
comments: false
bigimg: [{src: "/img/logo5Canular.png", desc: "Path"}]
---

CAhier NUmérique pour LAboratoire de recherche

Projet “CANULAR” soumis en avril 2021 à la commission d’évaluation des projets innovants et structurants. 
               aiguillage vers les pépinières numériques
 Financé par les pépinières numériques.

### Le projet

CANULAR : Cahier Numérique pour LAboratoire de Recheche

Les cahiers de laboratoire  électroniques (ELN) sont importants dans l’ouverture et la reproductibilité de la science. 

Car : Traçabilité, partage de savoir faire, données interopérables.... 

Frein actuel : Complexité d’utilisations des outils numériques actuels, matériel physique pas forcément adapté.

Beaucoup d’initiatives lancées mais pas d’outils faisant consensus dans l’institut et à l’extérieur. 

Objectif de CANULAR : Interfacer un ELN existant avec une liseuse électronique grand public.



